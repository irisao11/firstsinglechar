package main.first.unique.character;

import java.util.Arrays;
// creating a class that returns the first single char in a text
public class FirstUniqueChar {
    public char displayFirstChar(String myText) {
        // initializing the return char with a value that doesn't appear in a normal text
        char firstUniqueChar = '*';
        //creating an array of chars that contains my text
        char[] myTextArray = myText.toLowerCase().toCharArray();
        //creating an array of ints where I count  the number of times every char appears in my text
        int[] counts = new int[myTextArray.length];
        // an int to sum up the number of times a char appears in a text
        int count = 0;
        //initializing counts[]
        for (int i = 0; i < myTextArray.length; i++) {
            for (int j = 0; j < myTextArray.length; j++) {
                if (myTextArray[i] == myTextArray[j]) {
                    count = count + 1;
                }
            }
            counts[i] = count;
            count = 0;
        }
        System.out.println(Arrays.toString(counts));

        // loop to find the first single char in the text.
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] == 1) {
                System.out.println("First single character in the text is " + myTextArray[i]);
                System.out.println("First single character is the " + (i + 1) + " in the text");
                firstUniqueChar = myTextArray[i];
                break;
            }
        }

        System.out.println(firstUniqueChar);
        return firstUniqueChar;
    }

}
