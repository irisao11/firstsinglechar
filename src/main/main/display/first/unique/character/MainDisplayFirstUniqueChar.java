package main.main.display.first.unique.character;

import main.first.unique.character.FirstUniqueChar;

import java.util.Scanner;

public class MainDisplayFirstUniqueChar {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Your text: ");
        String myText = s.nextLine();
        FirstUniqueChar firstUniqueChar = new FirstUniqueChar();
        firstUniqueChar.displayFirstChar(myText);
    }
}
