package test.test.first.single.character;

import main.first.unique.character.FirstUniqueChar;
import org.junit.Assert;
import org.junit.Test;

public class testFirstSingleChar {
    //testing a text with unique characters
    @Test
    public void getFirstCharTest() {
        System.out.println(" method getFirstCharTest was called");
        FirstUniqueChar firstUniqueChar = new FirstUniqueChar();
        char myCharTest = firstUniqueChar.displayFirstChar("arab");
        Assert.assertEquals('r', myCharTest);
    }

    //testing a text with no unique characters
    @Test
    public void getFirstCharTestTwo() {
        System.out.println(" method getFirstCharTest was called");
        FirstUniqueChar firstUniqueChar = new FirstUniqueChar();
        char myCharTest = firstUniqueChar.displayFirstChar("aarr");
        Assert.assertEquals('*', myCharTest);
    }


}
